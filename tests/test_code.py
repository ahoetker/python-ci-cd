from cool_package.code import square, cube


def test_square():
    assert square(2) == 4
    assert square(-2) == 4


def test_cube():
    assert cube(2) == 8
    assert cube(-2) == -8
