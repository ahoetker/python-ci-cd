from math import isclose
from cool_package.floats import square, cube


def test_square():
    assert isclose(square(2.0), 4.0)
    assert isclose(square(-2.0), 4.0)


def test_cube():
    assert isclose(cube(2.0), 8.0)
    assert isclose(cube(-2.0), -8.0)

