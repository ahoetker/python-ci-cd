def square(x: float) -> float:
    return x * x


def cube(x: float) -> float:
    return x * x * x
