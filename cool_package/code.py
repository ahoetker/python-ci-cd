def square(x: int) -> int:
    return x * x


def cube(x: int) -> int:
    return x * x * x
